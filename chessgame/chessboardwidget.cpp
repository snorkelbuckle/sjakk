#include "chessboardwidget.h"

void ChessBoardWidget::CalculateSquareColors()
{
    for (int file = 0; file < 8; file++)
    {
        for (int rank = 0; rank < 8; rank++)
        {
            SetSquareBrush(file, rank);
        }
    }
}

ChessBoardWidget::ChessBoardWidget(QWidget *parent) : QWidget(parent)
{
    orientation = BoardOrientation::WHITEONBOTTOM;
    movestyle = MoveInputStyle::DRAGMOVE;
    moveStarted = false;
    //moveEnded = false;

    showIdentity = false;

    darkSquareBrush.setColor(Qt::darkGreen);
    darkSquareBrush.setStyle(Qt::SolidPattern);
    lightSquareBrush.setColor(Qt::white);
    lightSquareBrush.setStyle(Qt::SolidPattern);
    boardRect.setRect(0,0,0,0);
    squareRect.setRect(0,0,0,0);

    CalculateSquareDimensions();
    CalculateSquareColors();
}

void ChessBoardWidget::setIdentity(bool showident)
{
    showIdentity = showident;
    update();
}

bool ChessBoardWidget::getIdentity()
{
    return showIdentity;
}

void ChessBoardWidget::SetSquareBrush(int file, int rank)
{
    if (file % 2 == 0)
    {
        if (rank %2 == 0)
        {
            boardSquares[file][rank].squareBrush = &darkSquareBrush;
        }
        else
        {
            boardSquares[file][rank].squareBrush = &lightSquareBrush;
        }
    }
    else
    {
        if (rank %2 == 0)
        {
            boardSquares[file][rank].squareBrush = &lightSquareBrush;
        }
        else
        {
            boardSquares[file][rank].squareBrush = &darkSquareBrush;
        }
    }
}

void ChessBoardWidget::DrawSquare(QPainter *painter, int file, int rank)
{
    painter->setBrush(*boardSquares[file][rank].squareBrush);
    painter->drawRect(boardSquares[file][rank].squareRect);
}

void ChessBoardWidget::DrawIdentity(QPainter *painter, int file, int rank)
{
    painter->drawText(boardSquares[file][rank].squareRect.x() + 4,boardSquares[file][rank].squareRect.y() + boardSquares[file][rank].squareRect.height() - 4, GetSquareID(file, rank));
}

QString ChessBoardWidget::GetSquareID(int file, int rank)
{
    char firstfile = 'a';
    char firstrank = '1';


    int filecode = firstfile;
    filecode += file;
    int rankcode = firstrank;
    rankcode += rank;

    return QString("%1%2").arg((char)filecode).arg((char)rankcode);
}

void ChessBoardWidget::DrawChessBoard(QPainter *painter)
{
    for (int file = 0; file < 8; file++)
    {
        for (int rank = 0; rank < 8; rank++)
        {
            DrawSquare(painter, file, rank);
            if (showIdentity)
            {
                DrawIdentity(painter, file, rank);
            }
        }
    }
}

void ChessBoardWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);  // can this be stored globally when widget is instantiated?
                             // will it improve paint performance?
    painter.save();
    DrawChessBoard(&painter);
    painter.restore();
}

void ChessBoardWidget::CalculateSquareDimensions()
{
    for (int file = 0; file < 8; file++)
    {
        for (int rank = 0; rank < 8; rank++)
        {
            SetSquareRect(file, rank);
        }
    }
}

void ChessBoardWidget::resizeEvent(QResizeEvent *event)
{
    int ewidth = event->size().width();     // get the paint area width
    int eheight = event->size().height();   // get the paint area height

    // maintain equal width and height for each square, therefore
    // calculate x and y offset of the board to keep it centered in the
    // paint area

    int xoffset, yoffset;
    int boardsize;
    if (ewidth > eheight)
    {
        boardsize = eheight;
        xoffset = (ewidth - boardsize) / 2;
        yoffset = 0;
    }
    else
    {
        boardsize = ewidth;
        xoffset = 0;
        yoffset = (eheight - boardsize) / 2;
    }

    int squaresize = boardsize / 8;
    boardRect.setRect(xoffset,yoffset,boardsize,boardsize);
    squareRect.setRect(0,0,squaresize,squaresize);

    CalculateSquareDimensions();
}

void ChessBoardWidget::mousePressEvent(QMouseEvent *event)
{
    int file = -1;
    int rank = -1;

    if (event->button() == Qt::LeftButton)
    {
        if (FindSquareFromPos(event->pos(), file, rank))
        {
            if (movestyle == MoveInputStyle::DRAGMOVE)
            {
                moveStarted = true;
                emit MoveStarted(file, rank);
            }
            else
            {
                if (!moveStarted)
                {
                    moveStarted = true;
                    emit MoveStarted(file, rank);
                }
                else
                {
                    moveStarted = false;
                    emit MoveEnded(file, rank);
                }
            }
        }
        else
        {
            return;
        }
    }
}

void ChessBoardWidget::mouseReleaseEvent(QMouseEvent *event)
{
    int file = -1;
    int rank = -1;
    if (event->button() == Qt::LeftButton)
    {
        if (FindSquareFromPos(event->pos(), file, rank))
        {
            if (movestyle == MoveInputStyle::DRAGMOVE)
            {
                if (moveStarted)
                {
                    moveStarted = false;
                    emit MoveEnded(file, rank);
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
        else
        {
            return;
        }
    }
}

bool ChessBoardWidget::FindSquareFromPos(QPoint p, int &f, int &r)
{
    // TODO: make this more efficient, for now this is sufficient

    for (int file = 0; file < 8; file++)
    {
        for (int rank = 0; rank < 8; rank++)
        {
            if (boardSquares[file][rank].squareRect.contains(p.x(),p.y()))
            {
                f = file;
                r = rank;
                return true;
            }
        }
    }
    return false;
}


void ChessBoardWidget::flipBoard()
{
    if (orientation == BoardOrientation::WHITEONBOTTOM)
    {
        setOrientation(BoardOrientation::WHITEONTOP);
    }
    else
    {
        setOrientation(BoardOrientation::WHITEONBOTTOM);
    }
}

void ChessBoardWidget::setOrientation(BoardOrientation bo)
{
    orientation = bo;
    CalculateSquareDimensions();
    update();
}

BoardOrientation ChessBoardWidget::getOrientation()
{
    return orientation;
}

void ChessBoardWidget::SetSquareRect(int file, int rank)
{
    int x, y;
    int visualrank, visualfile;

    if (orientation == BoardOrientation::WHITEONBOTTOM)
    {
        visualrank = (-7 + rank) * -1;
        visualfile = file;
    }
    else
    {
        visualrank = rank;
        visualfile = (-7 + file) * -1;
    }

    x = visualfile * squareRect.width() + boardRect.x();
    y = visualrank * squareRect.height() + boardRect.y() ;
    boardSquares[file][rank].squareRect.setRect(x,y,squareRect.width(),squareRect.height());
}
