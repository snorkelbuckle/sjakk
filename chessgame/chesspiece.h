#ifndef CHESSPIECE_H
#define CHESSPIECE_H

#include <QObject>
#include <QPen>

enum class ChessPieceType
{
    PAWN, ROOK, KNIGHT, BISHOP, QUEEN, KING, NOPIECE
};

enum class ChessPieceColor
{
    WHITE, BLACK
};

class ChessPiece : public QObject
{
    Q_OBJECT
public:
    explicit ChessPiece(QObject *parent = nullptr);

    ChessPieceColor PieceColor;
    ChessPieceType PieceType;

    QString ForegroundText;
    QString BackgroundText;

    QPen ForegroundPen;
    QPen BackgroundPen;

signals:

public slots:
};

#endif // CHESSPIECE_H
