#include "chessgamemanager.h"

ChessGameManager::ChessGameManager(QObject *parent) : QObject(parent)
{
    statusbar = NULL;
    chessboard = NULL;
    moveNotation.first = "";
    moveNotation.second = "";
}

void ChessGameManager::FlipBoard()
{
    chessboard->flipBoard();
}

void ChessGameManager::IdentifySquares(bool showident)
{
    chessboard->setIdentity(showident);
}

void ChessGameManager::SetChessBoardWidget(ChessBoardWidget *cboard)
{
    chessboard = cboard;
    connect(chessboard, &ChessBoardWidget::MoveStarted, this, &ChessGameManager::MoveStarted);
    connect(chessboard, &ChessBoardWidget::MoveEnded, this, &ChessGameManager::MoveEnded);
}

void ChessGameManager::MoveStarted(int file, int rank)
{
    moveNotation = QPair<QString,QString>(chessboard->GetSquareID(file,rank),QString(""));
    statusbar->showMessage(QString("Move Started: %1").arg(moveNotation.first));
}

void ChessGameManager::MoveEnded(int file, int rank)
{
    moveNotation.second = (chessboard->GetSquareID(file,rank));
    statusbar->showMessage(QString("Move Ended: %1-%2").arg(moveNotation.first).arg(moveNotation.second));
}

void ChessGameManager::SetStatusBarWidget(QStatusBar *sbar)
{
    statusbar = sbar;
}
