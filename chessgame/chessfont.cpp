#include "chessfont.h"

ChessFont::ChessFont(QObject *parent) : QObject(parent)
{
    fontLoaded = false;
    fontPath = "";
}

void ChessFont::LoadChessFont()
{
    if (fontPath.isEmpty())
    {
        return;
    }

    fontID = QFontDatabase::addApplicationFont(fontPath);
    if (fontID == -1)
    {
        return;  // for now, we just fail silently.
    }
    fontLoaded = true;

    familyName = QFontDatabase::applicationFontFamilies(fontID).at(0);
    chessFont.setFamily(familyName);
}

QString ChessFont::getFontPath()
{
    return fontPath;
}

QString ChessFont::getFamily()
{
    return familyName;
}

QFont ChessFont::getChessFont()
{
    return chessFont;
}

void ChessFont::setFontPath(QString fpath)
{
    fontPath = fpath;
}
