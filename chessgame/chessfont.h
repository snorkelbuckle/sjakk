#ifndef CHESSFONT_H
#define CHESSFONT_H

#include <QObject>
#include <QFont>
#include <QString>
#include <QFontDatabase>

class ChessFont : public QObject
{
    Q_OBJECT
public:
    explicit ChessFont(QObject *parent = nullptr);
    void LoadChessFont();

    // getters
    QString getFontPath();
    QString getFamily();
    QFont getChessFont();

    // setters
    void setFontPath(QString fpath);

private:
    bool fontLoaded;
    int fontID;
    QString familyName;
    QString fontPath;
    QFont chessFont;

signals:

public slots:
};

#endif // CHESSFONT_H
