#ifndef CHESSBOARDWIDGET_H
#define CHESSBOARDWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QResizeEvent>
#include <QMouseEvent>
#include <QPoint>
#include <QDebug>

enum class BoardOrientation
{
    WHITEONBOTTOM, WHITEONTOP
};

enum class MoveInputStyle
{
    DRAGMOVE, LIFTANDDROP
};

typedef struct squarestruct
{
    QRect       squareRect;
    QBrush*     squareBrush;
} SquareStruct;

class ChessBoardWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ChessBoardWidget(QWidget *parent = nullptr);
    void setIdentity(bool showident);
    bool getIdentity();

    void flipBoard();
    void setOrientation(BoardOrientation bo);
    BoardOrientation getOrientation();
    QString GetSquareID(int file, int rank);

    void CalculateSquareColors();

signals:
    //void SquareMousePressed(int file, int rank);
    //void SquareMouseReleased(int file, int rank);
    //void PieceMove(int srcfile, int srcrank, int tgtfile, int tgtrank);
    void MoveStarted(int sourcefile, int sourcerank);
    void MoveEnded(int destfile, int destrank);

public slots:

private:
    QRect boardRect;
    QRect squareRect;
    QBrush darkSquareBrush;
    QBrush lightSquareBrush;
    SquareStruct boardSquares[8][8];
    BoardOrientation orientation;
    MoveInputStyle movestyle;
    bool moveStarted;
    //bool moveEnded;

    bool showIdentity;
    void SetSquareBrush(int file, int rank);
    void SetSquareRect(int file, int rank);
    void DrawChessBoard(QPainter *painter);
    void DrawSquare(QPainter *painter, int file, int rank);
    void DrawIdentity(QPainter *painter, int file, int rank);

    void CalculateSquareDimensions();
    bool FindSquareFromPos(QPoint pos, int &f, int &r);

protected:
    void paintEvent(QPaintEvent *) override;
    void resizeEvent(QResizeEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

};

#endif // CHESSBOARDWIDGET_H
