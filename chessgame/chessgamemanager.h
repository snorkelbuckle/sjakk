#ifndef CHESSGAMEMANAGER_H
#define CHESSGAMEMANAGER_H

#include <QObject>
#include <QWidget>
#include <QStatusBar>
#include "chessgame/chessfont.h"
#include "chessgame/chessboardwidget.h"
#include <QDebug>

class ChessGameManager : public QObject
{
    Q_OBJECT
public:
    explicit ChessGameManager(QObject *parent = nullptr);
    void SetChessBoardWidget(ChessBoardWidget* cboard);
    void SetStatusBarWidget(QStatusBar* sbar);

private:
    ChessFont chessSet;
    ChessBoardWidget* chessboard;
    QStatusBar* statusbar;
    QPair<QString,QString> moveNotation;

signals:

public slots:
    void FlipBoard();
    void IdentifySquares(bool showident);

    void MoveStarted(int file, int rank);
    void MoveEnded(int file, int rank);
    //void SquarePressed(int file, int rank);
    //void SquareReleased(int file, int rank);
};

#endif // CHESSGAMEMANAGER_H
