#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    gmanager.SetChessBoardWidget(ui->gameboardWidget);
    gmanager.SetStatusBarWidget(ui->statusBar);
    connect(ui->actionIdentify_Squares, &QAction::toggled, &gmanager, &ChessGameManager::IdentifySquares);
    connect(ui->actionFlip_Board, &QAction::triggered, &gmanager, &ChessGameManager::FlipBoard);
}

MainWindow::~MainWindow()
{
    delete ui;
}
