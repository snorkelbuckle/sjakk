# Sjakk

Chess client for FICS and ICC.  I realize there are other clients out there
that are mature and work very well, even if they are no longer updated. But
I like coding and I wanted a project, so here I am.

I thought I had a vision for Sjakk to be very simple client.  I decided not to 
preclude anything and changed my mind.  At the moment, it will become whatever
I need it to be.  If you'd like to change that, make some requests and test
the beta versions as they come out.  Better yet, if you can code too, help out!
The one thing I thought peculiar is that there is no open source chess client that
has a vibrant community of developers.  Most likely because there aren't many
chess players who are coders too, I suppose.


Just some general notes:

- I've become tired of developing for windows using C# and .Net. The technology 
within MS is constantly changing and I don't want to deal with.  Case in point, if you
made something for Windows Phone 8 (which I did), then guess what, your windows
store APP gets pulled if you didn't make a version for Windows 10, even though
the WIN8 app is perfectly compatible with WIN10 and installs on WIN10 phone.

- I like coding and chess, my two favorite things.  I needed a project so here
I am.

- Going down the road of cross platform code using Qt framework with C++.  I 
thought about trying a new programming language like Go or Rust, both seem
interesting, but couldn't find a good Gui library to go with them.  They have Qt
bindings but looked like too much work.  Why use a Qt binding that is buggy when
I can use the real thing in C++.  I did try Piston for Rust, went through their sample
tutorial, makes a nice spinning red square in a window with bright green background.
One problem, the performance sucked really bad.  Just not ready for prime-time.

- Since this is going to be cross platform, will consider mobile versions
provided the dev setup is not too difficult.  Cross-platform doesn't necessarily
mean that it compiles perfectly on another platform.

- Considering an SDK for plugins and modular design. At the moment I have no
idea what the SDK would be, so probably what will happen is that I will develop
the client, then later decide to add an SDK and then will realize I have to
rewrite most of the code to support a plugin architecture!  And of course, the irony
will be that I will discover that Rust or Go (see above bullet) will be the best
way to make an SDK...  I know this is backwards, but I'm not planning on adding
SDK feature yet.  BTW, adding SDK means I won't be creating any plugins except
maybe for the first example plugin. If this project takes off, not holding my
breath, then maybe the community can add their own plugins.


